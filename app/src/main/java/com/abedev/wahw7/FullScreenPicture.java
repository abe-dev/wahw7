package com.abedev.wahw7;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

/**
 * Created by home on 28.09.2016.
 */

public class FullScreenPicture extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context,FullScreenPicture.class);
    }

}
