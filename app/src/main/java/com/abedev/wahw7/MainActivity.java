package com.abedev.wahw7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn1:
                intent = ObjectAnimatorActivity.getStartIntent(this);
                break;
            case R.id.btn2:
                intent = TransitionAnimationActivity.getStartIntent(this,TransitionAnimationActivity.TRANSITION_CASE_FRAGMENT);
                break;
            case R.id.btn3:
                intent = TransitionAnimationActivity.getStartIntent(this,TransitionAnimationActivity.TRANSITION_CASE_ACTIVITY);
                break;
            case R.id.btn4:
                intent = VectorAnimationActivity.getStartIntent(this);
                break;
            default:
                intent = null;

        }
        if (intent!=null)
            startActivity(intent);
    }
}
