package com.abedev.wahw7;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by home on 28.09.2016.
 */

public class CustomFragment extends Fragment {
    private static String ARG_ORIENTATION = "orientation";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, container, false);
        //noinspection WrongConstant
        ((LinearLayout) view).setOrientation(getArguments().getInt(ARG_ORIENTATION));
        return view;
    }

    public static CustomFragment create(@LinearLayoutCompat.OrientationMode int orientation) {
        Bundle args = new Bundle();
        args.putInt(ARG_ORIENTATION,orientation);
        CustomFragment fragment = new CustomFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
