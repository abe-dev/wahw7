package com.abedev.wahw7;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.transition.AutoTransition;
import android.view.View;
import android.widget.LinearLayout;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by home on 27.09.2016.
 */

public class TransitionAnimationActivity extends AppCompatActivity {
    private static final String EXTRA_CASE = "case";

    @IntDef({TRANSITION_CASE_FRAGMENT,TRANSITION_CASE_ACTIVITY})
    @Retention(SOURCE)
    public @interface TransitionCase {}
    public static final int TRANSITION_CASE_FRAGMENT = 0;
    public static final int TRANSITION_CASE_ACTIVITY = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        if (savedInstanceState==null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment,CustomFragment.create(LinearLayout.HORIZONTAL))
                    .commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("dummy",0);
    }
    public static Intent getStartIntent(Context context,@TransitionCase int transitionCase) {
        Intent intent = new Intent(context, TransitionAnimationActivity.class);
        intent.putExtra(EXTRA_CASE,transitionCase);
        return intent;
    }

    public void onClick(View view) {
        @TransitionCase  int transitionCase = getIntent().getIntExtra(EXTRA_CASE,-1);
        switch (transitionCase) {
            case TRANSITION_CASE_ACTIVITY:
                if (view.getId() == R.id.img) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, "img");
                    startActivity(FullScreenPicture.getStartIntent(this), options.toBundle());
                }
                break;
            case TRANSITION_CASE_FRAGMENT:
                if (view.getId() == R.id.btn && getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    CustomFragment fragment = CustomFragment.create(LinearLayout.VERTICAL);
                    fragment.setSharedElementEnterTransition(new AutoTransition());
                    fragment.setSharedElementReturnTransition(new AutoTransition());
                    getSupportFragmentManager()
                            .beginTransaction()
                            .addSharedElement(findViewById(R.id.img), "img")
                            .addSharedElement(findViewById(R.id.text), "text")
                            .addSharedElement(findViewById(R.id.btn), "btn")
                            .replace(R.id.fragment, fragment)
                            .addToBackStack(null)
                            .commit();
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown transition case!");
        }
    }


}

