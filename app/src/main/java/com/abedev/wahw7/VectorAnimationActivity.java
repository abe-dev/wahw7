package com.abedev.wahw7;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by home on 27.09.2016.
 */

public class VectorAnimationActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vector_animation);
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context,VectorAnimationActivity.class);
    }
    public void onClick(View view) {
        Animatable drawable = (Animatable) ((ImageView) view).getDrawable();
        drawable.start();
    }

}
