package com.abedev.wahw7;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.ArcMotion;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.android.unsplash.transition.TextResize;

/**
 * Created by home on 27.09.2016.
 */

public class ObjectAnimatorActivity extends AppCompatActivity {

    public static final int NEW_TEXT_SIZE = 12;
    public static final int NEW_HEIGTH = 100;
    public static final int NEW_WIDTH = 150;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_animator);
    }
    public void onClick(View view) {
        if (view.getId() == R.id.btn) {
            final Button button = (Button) view;
            TransitionSet set = new TransitionSet();
            Transition cbTransition = new ChangeBounds();
            cbTransition.setPathMotion(new ArcMotion());
            cbTransition.setInterpolator(new AccelerateInterpolator());
            set.addTransition(cbTransition);
            set.addTransition(new TextResize());

            TransitionManager.beginDelayedTransition((ViewGroup) view.getParent(),set);

            final FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) button.getLayoutParams();
            layoutParams.gravity = Gravity.END|Gravity.TOP|Gravity.RIGHT;
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, NEW_HEIGTH, displayMetrics);
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, NEW_WIDTH, displayMetrics);
            button.setLayoutParams(layoutParams);
            button.setTextColor(0xffff0000);
            button.setTextSize(TypedValue.COMPLEX_UNIT_SP, NEW_TEXT_SIZE);
        }
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context,ObjectAnimatorActivity.class);
    }
}
